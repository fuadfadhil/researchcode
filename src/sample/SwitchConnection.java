package sample;

public class SwitchConnection {

    String connection = "";
    int totalDataFlow = 0;

    public SwitchConnection(String connection, int totalDataFlow) {
        this.connection = connection;
        this.totalDataFlow = totalDataFlow;
    }

    public String getConnection() {
        return connection;
    }

    public int getTotalDataFlow() {
        return totalDataFlow;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public void setTotalDataFlow(int totalDataFlow) {
        this.totalDataFlow = totalDataFlow;
    }
}
