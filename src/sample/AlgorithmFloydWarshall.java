package sample;

import java.util.Scanner;

public class AlgorithmFloydWarshall extends Main{

    //Preparing Matrix
    //Matrix for each vertices
    static Integer matrixVerticesWeight [][] = new Integer[totalSwitch][totalSwitch];

    //Matrix for route Path
    static String matrixPathMap [][] = new String[totalSwitch][totalSwitch];

    //Matrix for switch and host connection
    static String matrixHostAndSwitchConnection []  = new String[totalHosts];

    public static void mainShortestPathSearching(){
        //Main Method for Searching Shortest Path using FloydWarshall Algorithm

        setTopologyMatrix();
        setMatrixHostAndSwitchConnection();
        shortestPathSearching();
    }

    //Setting weight and connection of the Topology
    public static void setTopologyMatrix(){

        Scanner input = new Scanner(System.in);

        System.out.println("Setting Topology and Weight");
        System.out.println("Set value 0 if the source and destination is same");
        System.out.println("or if there is no direct connection");

        //Make Matrix Top Label
        System.out.print(" \nMatrix Weight \n ");
        for (int labelTop = 65 ; labelTop % 65 < totalSwitch ; labelTop++){
            System.out.print(" "+((char)labelTop));
        }

        System.out.println("");

        //Input Matrix Topology Weight and Connection
        for (int switchSource = 0 ; switchSource<totalSwitch ; switchSource++){
            System.out.print((char)(switchSource+65)+ " ");
            for (int switchDestination = 0 ; switchDestination<totalSwitch; switchDestination++){
                if (matrixVerticesWeight[switchSource][switchDestination] != null) //if the connection is already have value
                    System.out.print(matrixVerticesWeight[switchSource][switchDestination] + " ");
                else if (switchSource == switchDestination) {
                    matrixVerticesWeight[switchSource][switchDestination] = 0;
                    matrixPathMap[switchSource][switchDestination] = "0";
                    System.out.print(matrixVerticesWeight[switchSource][switchDestination] + " ");
                }else {
                    matrixVerticesWeight[switchSource][switchDestination] = input.nextInt();
                    matrixVerticesWeight[switchDestination][switchSource] = matrixVerticesWeight[switchSource][switchDestination]; //set Same Value for the opposite route

                    //add connection to arrayList for distribution table
                    switchConnections.add(new SwitchConnection(String.valueOf((char)(switchSource+65))+ String.valueOf((char)(switchDestination+65)),0));

                    //If the input value is 0, its assume that there's no path
                    if (matrixVerticesWeight[switchSource][switchDestination]==0){
                        matrixPathMap[switchSource][switchDestination] = "0";// 0 => there's no direct path
                        matrixPathMap[switchDestination][switchSource] = "0";
                    }else {
                        matrixPathMap[switchSource][switchDestination] = "" + (char) (switchSource + 65);
                        matrixPathMap[switchDestination][switchSource] = "" + (char) (switchDestination + 65);
                    }
                }
            }
        }
    }

    public static void printMatrix(){

        for (int count = 0 ; count < 2 ; count++) {

            System.out.println("");

            if (count == 0)
                System.out.println("\nMatrix Weight \n ");
            else
                System.out.println("\nMatrix PathMap \n ");
            //Print Matrix PathMap
            System.out.print(" ");
            for (int labelTop = 65 ; labelTop % 65 < totalSwitch ; labelTop++){
                System.out.print(" "+((char)labelTop));
            }

            System.out.println("");

            for (int source = 0; source < totalSwitch; source++) {
                System.out.print((char) (source + 65) + " ");
                for (int destination = 0; destination < totalSwitch; destination++) {
                    if (count == 0)
                        System.out.print(AlgorithmFloydWarshall.matrixVerticesWeight[source][destination] + " ");
                    else
                        System.out.print(AlgorithmFloydWarshall.matrixPathMap[source][destination] + " ");
                }
                System.out.println("");
            }
        }
        System.out.println("");
    }

    //Setting switch and Host Connection
    public static void setMatrixHostAndSwitchConnection(){

        Scanner input = new Scanner(System.in);

        System.out.println("");
        System.out.println("Setting Host to Switch Connection");
        System.out.println("Input the Switch Alphabet Name");

        //Make Matrix Top Label

        for (int labelTop = 0 ; labelTop < totalHosts ; labelTop++){
            System.out.print(labelTop + " ");
        }

        System.out.println();

        for (int host = 0 ; host < totalHosts ; host++){
            matrixHostAndSwitchConnection[host] = input.next();
        }
        System.out.println();

        System.out.println("Starting Matrix");
        printMatrix();
    }

    //Start Searching Shortest Path
    public static void shortestPathSearching(){

        int totalDistance; //variable to save the total distance between switch

        System.out.println(" Searching Shortest Path Matrix");
        int stopPoint; //Define StopPoint for path searching, 0 => A as StopPoint

        for (stopPoint = 0 ; stopPoint < totalSwitch ; stopPoint++  ){
            for (int switchSource = 0 ; switchSource < totalSwitch ; switchSource++){
                for (int switchDestination = 0 ; switchDestination < totalSwitch ; switchDestination++) {
                    System.out.println(" " + switchSource + "," + switchDestination);
                    System.out.println("Iteration " + (stopPoint+1) + "\t Stop Point " + (char)(stopPoint+65));
                    printMatrix();
                    //if the the source and destination is same or stop point and destination is same = continue
                    if (matrixPathMap[switchSource][stopPoint].equals("0")){
                        break;
                    //if the source to stop point switch is 0 / not connected increase the switch source
                    }else if (switchSource == switchDestination || stopPoint == switchSource || stopPoint == switchDestination){
                        continue;
                    }else{
                        //switch source is connected to destination by stop point
                        totalDistance = 0;
                        if (!matrixPathMap[stopPoint][switchDestination].equals("0")){ //stopPoint -> destination connected
                            totalDistance += matrixVerticesWeight[switchSource][stopPoint] + matrixVerticesWeight[stopPoint][switchDestination];
                            if (totalDistance < matrixVerticesWeight[switchSource][switchDestination]||matrixVerticesWeight[switchSource][switchDestination]==0)
                                matrixVerticesWeight[switchSource][switchDestination] = totalDistance;
                            else
                                continue;
                        }else if (matrixPathMap[stopPoint][switchDestination].equals("0")){//stopPoint -> destination not connected
                            continue;
                        }
                        //set value pathMap switchSource -> switchDestionation as stopPoint in Alphabet Char
                        if (matrixPathMap[switchSource][stopPoint].equals(""+(char)(switchSource+65)))
                            matrixPathMap[switchSource][switchDestination] = "" + (char)(stopPoint+65);
                        else
                            matrixPathMap[switchSource][switchDestination] = matrixPathMap[switchSource][stopPoint];
                    }
                }
            }
        }
    }

    //method to make the distribution table
    public static void distributionTable(){

        //looping to count how many data flow for each vertices
        for (int source = 0 ; source<totalSwitch ; source++){
            int destination = 1+source; //we dont need to check all the matrix, just top right triangle of the matrix
            while (destination < totalSwitch){
                if (matrixVerticesWeight[source][destination] != 0){

                }
                destination++;
            }
        }
    }
}