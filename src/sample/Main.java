package sample;

import com.sun.org.apache.regexp.internal.StreamCharacterIterator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    //Preparing total host and switch
    static int totalHosts = 5;
    static int totalSwitch = 6; //variabel to save total vertices in the

    static ArrayList <SwitchConnection> switchConnections = new ArrayList<>();

    public static void main(String[] args) {
        //launch(args);
        System.out.println("Starting Program.........");

        //starting FloydWarshall Algorithm
        AlgorithmFloydWarshall.mainShortestPathSearching();

        resetVariable();
    }

    static void resetVariable(){
        totalSwitch = 6;
        totalHosts = 5;
        switchConnections.clear();
    }
}
